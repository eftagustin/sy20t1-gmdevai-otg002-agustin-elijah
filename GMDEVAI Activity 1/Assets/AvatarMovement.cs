﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarMovement : MonoBehaviour
{
    public float walkspeed = 3;

    Vector3 forward = new Vector3(0,1,0);
    Vector3 right = new Vector3(1,0,0);

    Rigidbody rigidBody;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Walk using WASD
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.forward * walkspeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.back * walkspeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left * walkspeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * walkspeed * Time.deltaTime);
        }


    }
}
