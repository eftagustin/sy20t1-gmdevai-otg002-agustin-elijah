﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonSpawn : MonoBehaviour
{

    public GameObject obstacleFlee;
    public GameObject obstacleFlock;
    GameObject[] agents;

    // Start is called before the first frame update
    void Start()
    {
        agents = GameObject.FindGameObjectsWithTag("agent");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if(Physics.Raycast(ray.origin, ray.direction, out hit))
            {
                Instantiate(obstacleFlee, hit.point, obstacleFlee.transform.rotation);
                foreach (GameObject a in agents){
                    a.GetComponent<AIControl>().DetectNewObstacle(hit.point);
                }
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray.origin, ray.direction, out hit))
            {
                Instantiate(obstacleFlock, hit.point, obstacleFlock.transform.rotation);
                foreach (GameObject a in agents)
                {
                    a.GetComponent<AIControl>().DetectNewObstacleFlock(hit.point);
                }
            }
        }
    }
}
