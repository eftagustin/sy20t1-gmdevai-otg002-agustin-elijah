﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointFollow : MonoBehaviour
{


    //public GameObject[] waypoints;

    public UnityStandardAssets.Utility.WaypointCircuit circuit;

    float speed = 5;
    float rotSpeed = 3;
    float accuracy = 1;

    int currentWaypointIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        //waypoints = GameObject.FindGameObjectsWithTag("Waypoint");
    }

    // Update is called once per frame
    void Update()
    {


        if (circuit.Waypoints.Length == 0) return;

        GameObject currentWaypoint = circuit.Waypoints[currentWaypointIndex].gameObject;

        Vector3 lookAtGoal = new Vector3(currentWaypoint.transform.position.x,
                                          currentWaypoint.transform.position.y,
                                          currentWaypoint.transform.position.z);

        //to compute direction
        Vector3 direction = lookAtGoal - this.transform.position;


        if(direction.magnitude < 1.0)
        {
            currentWaypointIndex++;

            if(currentWaypointIndex >= circuit.Waypoints.Length)
            {
                currentWaypointIndex = 0;
            }
        }

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
               Quaternion.LookRotation(direction),
               Time.deltaTime * rotSpeed);

        this.transform.Translate(0, 0, speed * Time.deltaTime);
    }
}
