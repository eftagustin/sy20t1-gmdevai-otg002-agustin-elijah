﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Agent3_AI : AIControl
{
    
    void Start()
    {
        this.agent = this.GetComponent<NavMeshAgent>();
        playerMovement = target.GetComponent<WASDMovement>();
        range = 5.0f;
    }

    
    void Update()
    {
        CheckDistance();
        DoAction();
    }

    void CheckDistance()
    {
        ///if target is near
        ///evade
        ///if not, wander
        ///

        if (Vector3.Distance(target.transform.position, this.transform.position) < range)
        {
            Debug.Log(name + " has switched state");
            currentState = State.Evading;
        }
        else
        {
            currentState = State.Wandering;
        }
    }
}
