﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class Agent2_AI : AIControl
{
    
    void Start()
    {
        this.agent = this.GetComponent<NavMeshAgent>();
        playerMovement = target.GetComponent<WASDMovement>();
        range = 10.0f;
    }

    
    void Update()
    {
        CheckDistance();
        DoAction();
    }

    void CheckDistance()
    {
        ///if target is near
        ///clever hide
        ///if not, wander
        
        if (Vector3.Distance(target.transform.position, this.transform.position) < range)
        {
            Debug.Log(name + " has switched state");
            currentState = State.CleverHiding;
        }
        else
        {
            currentState = State.Wandering;
        }
    }
}
