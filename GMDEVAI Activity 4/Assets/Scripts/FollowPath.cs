﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPath : MonoBehaviour
{
    Transform goal;

    [SerializeField] private GameObject greenTank;
    [SerializeField] private GameObject redTank;
    [SerializeField] private GameObject blueTank;

    [SerializeField] private GameObject[] tanks;

    float speed = 5.0f;
    float accuracy = 1.0f;
    float rotSpeed = 2.0f;

    public GameObject wpManager;
    GameObject[] wps;

    GameObject currentNode;

    int currentWaypointIndex = 0;

    Graph graph;

    FollowPath redComp;
    FollowPath blueComp;
    FollowPath greenComp;

    int activeTank = 0;

    void Start()
    {
        activeTank = 1;
        wps = wpManager.GetComponent<WaypointManager>().waypoints;
        graph = wpManager.GetComponent<WaypointManager>().graph;
        currentNode = wps[0];

        redComp = redTank.GetComponent<FollowPath>();
        greenComp = greenTank.GetComponent<FollowPath>();
        blueComp = blueTank.GetComponent<FollowPath>();
    }

    private void Update()
    {
        SetTanksStatus();
    }

    void LateUpdate()
    {
        if(graph.getPathLength() == 0 || currentWaypointIndex == graph.getPathLength())
        {
            return;
        }

        //where we currently are
        currentNode = graph.getPathPoint(currentWaypointIndex);

        //if we are close enough to current, go to next one
        if(Vector3.Distance(graph.getPathPoint(currentWaypointIndex).transform.position, 
                            transform.position) < accuracy)
        {
            currentWaypointIndex++;
        }

        if(currentWaypointIndex < graph.getPathLength())
        {
            goal = graph.getPathPoint(currentWaypointIndex).transform;
            Vector3 lookAtGoal = new Vector3(goal.position.x, transform.position.y, goal.position.z);
            Vector3 direction = lookAtGoal - this.transform.position;

            this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                       Quaternion.LookRotation(direction),
                                                       Time.deltaTime * rotSpeed);

            this.transform.Translate(0, 0, speed * Time.deltaTime);
        }

    }

    public void SetGreenActive()
    {
        activeTank = 1;
    }

    public void SetBlueActive()
    {
        activeTank = 3;
    }

    public void SetRedActive()
    {
        activeTank = 2;
    }

    public void SetTanksStatus()
    {
        if(activeTank == 1)
        {
            greenComp.enabled = true;
            redComp.enabled = false;
            blueComp.enabled = false;
        }
        else if (activeTank == 2)
        {
            greenComp.enabled = false;
            redComp.enabled = true;
            blueComp.enabled = false;
        }
        else if (activeTank == 3)
        {
            greenComp.enabled = false;
            redComp.enabled = false;
            blueComp.enabled = true;

        }else if(activeTank == 0)
        {
            greenComp.enabled = true;
            redComp.enabled = false;
            blueComp.enabled = false;
        }
    }

    public void GoToHelipad()
    {
        graph.AStar(currentNode, wps[0]);
        currentWaypointIndex = 0;
    }
    
    public void GoToRuins()
    {
        graph.AStar(currentNode, wps[5]);
        currentWaypointIndex = 0;
    }

    public void GoToFactory()
    {
        graph.AStar(currentNode, wps[7]);
        currentWaypointIndex = 0;
    }

    public void GoToTwinMountains()
    {
        graph.AStar(currentNode, wps[9]);
        currentWaypointIndex = 0;
    }


    public void GoToBarracks()
    {
        graph.AStar(currentNode, wps[10]);
        currentWaypointIndex = 0;
    }


    public void GoToCommandCenter()
    {
        graph.AStar(currentNode, wps[11]);
        currentWaypointIndex = 0;
    }


    public void GoToOilRefinery()
    {
        graph.AStar(currentNode, wps[14]);
        currentWaypointIndex = 0;
    }


    public void GoToTankers()
    {
        graph.AStar(currentNode, wps[15]);
        currentWaypointIndex = 0;
    }


    public void GoToRadar()
    {
        graph.AStar(currentNode, wps[13]);
        currentWaypointIndex = 0;
    }


    public void GoCommandPost()
    {
        graph.AStar(currentNode, wps[12]);
        currentWaypointIndex = 0;
    }


    public void GoToMiddleMap()
    {
        graph.AStar(currentNode, wps[8]);
        currentWaypointIndex = 0;
    }

}
  
//physics update
//movt late update