﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentManager : MonoBehaviour
{
    GameObject[] agents;
    public float stoppingDistance;
    public Transform target;


    void Start()
    {
        agents = GameObject.FindGameObjectsWithTag("AI");
        stoppingDistance = 1.0f;
    }

    
    void Update()
    {

        foreach (GameObject ai in agents)
        {
            if ((Vector3.Distance(ai.transform.position, target.transform.position) > stoppingDistance))
                ai.GetComponent<AIControl>().agent.SetDestination(target.position);
            }
        }
    }


